package base;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Enumeration;

public class OOMTest {
    public static void main(String[] args) {

//        Byte[] OneM1 = new Byte[1024*1024];
//        Byte[] OneM2 = new Byte[1024];
        System.out.println("1235");
        System.out.println(getLocalIp());
    }

    public static String getLocalIp() {
        try {
            Enumeration<NetworkInterface> netInterfaces = NetworkInterface.getNetworkInterfaces();
            String ip = null;

            while(netInterfaces.hasMoreElements()) {
                NetworkInterface ni = (NetworkInterface)netInterfaces.nextElement();
                Enumeration ips = ni.getInetAddresses();

                while(ips.hasMoreElements()) {
                    InetAddress ipObj = (InetAddress)ips.nextElement();
                    if (ipObj.isSiteLocalAddress()) {
                        ip = ipObj.getHostAddress();
                        return ip;
                    }
                }
            }

            return ip;
        } catch (Exception var5) {
            return null;
        }
    }
}
