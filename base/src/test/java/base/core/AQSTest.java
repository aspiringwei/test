package base.core;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class AQSTest {

    public static void main(String[] args) {
        Lock mutex = new Mutex();
        Condition condition = mutex.newCondition();
        mutex.lock();
        new Thread(() -> {
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            // blocking
            for (;;) {
                if (mutex.tryLock()) {
                    break;
                }
            }
            System.out.println(Thread.currentThread() + ":"+ mutex.tryLock());
            condition.signal();
            mutex.unlock();

        }).start();

        new Thread(() -> {
            try {
                TimeUnit.SECONDS.sleep(3);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread() + ":"+ mutex.tryLock());
            mutex.unlock();
            System.out.println(Thread.currentThread() + ":release lock");
        }).start();

        System.out.println(Thread.currentThread() + ":main wait");
        try {
            condition.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread() + ":notify main");
        mutex.unlock();
    }
}
